package paddle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	timeout     = 20 * time.Second
	idleTimeout = 90 * time.Second
	endpoint    = "https://vendors.paddle.com/api/2.0"
)

// Client holds information about a Paddle client
type Client struct {
	client    *http.Client
	vendorID  string
	authToken string
}

// NewClient creates a new Paddle client for given auth code
func NewClient(vendorID, authToken string) *Client {
	return &Client{
		client: &http.Client{
			Timeout: timeout,
			Transport: &http.Transport{
				IdleConnTimeout: idleTimeout,
			},
		},
		vendorID:  vendorID,
		authToken: authToken,
	}
}

// CancelSubscription cancels a subscription
func (c *Client) CancelSubscription(subsID string) error {
	var (
		resp struct {
			Success bool `json:"success"`
			Error   struct {
				Message string `json:"message"`
			} `json:"error"`
		}
	)

	form := url.Values{}
	form.Add("subscription_id", subsID)

	r, err := c.fetch(
		http.MethodPost,
		"/subscription/users_cancel",
		&form,
	)
	if err != nil {
		return err
	}

	err = json.Unmarshal(r, &resp)
	if err != nil {
		return err
	}

	if !resp.Success {
		return fmt.Errorf(resp.Error.Message)
	}
	return nil
}

// ReschedulePayment postpones the payment date for given days. Days can be negative
func (c *Client) ReschedulePayment(subsID string, daysLeft float64) error {
	var (
		resp struct {
			Success bool `json:"success"`
			Error   struct {
				Message string `json:"message"`
			} `json:"error"`
		}
	)

	// We put 1 day of slack just in case
	days := int(math.Floor(daysLeft)) - 1
	if days <= 0 {
		return nil
	}

	date, payID, err := c.nextPaymentDate(subsID)
	if err != nil {
		return err
	}
	newDate := date.Add((time.Duration(days) * 24 * time.Hour))

	form := url.Values{}
	form.Add("payment_id", strconv.Itoa(payID))
	form.Add("date", newDate.Format("2006-01-02"))

	r, err := c.fetch(
		http.MethodPost,
		"/subscription/payments_reschedule",
		&form,
	)
	if err != nil {
		return err
	}

	err = json.Unmarshal(r, &resp)
	if err != nil {
		return err
	}

	if !resp.Success {
		return fmt.Errorf(resp.Error.Message)
	}
	return nil
}

func (c *Client) nextPaymentDate(subsID string) (time.Time, int, error) {
	var (
		resp struct {
			Success bool `json:"success"`
			Error   struct {
				Message string `json:"message"`
			} `json:"error"`
			Response []struct {
				ID         int    `json:"id"`
				PayoutDate string `json:"payout_date"`
			} `json:"response"`
		}
	)

	form := url.Values{}
	form.Add("subscription_id", subsID)
	form.Add("is_paid", "0")

	r, err := c.fetch(
		http.MethodPost,
		"/subscription/payments",
		&form,
	)
	if err != nil {
		return time.Time{}, 0, err
	}

	err = json.Unmarshal(r, &resp)
	if err != nil {
		return time.Time{}, 0, err
	}

	if !resp.Success {
		return time.Time{}, 0, fmt.Errorf(resp.Error.Message)
	}

	if len(resp.Response) < 1 {
		return time.Time{}, 0, fmt.Errorf("Couldn't find a recurring payment")
	}

	date, err := time.Parse("2006-01-02", resp.Response[0].PayoutDate)
	if err != nil {
		return time.Time{}, 0, err
	}

	return date, resp.Response[0].ID, nil
}

func (c *Client) fetch(method, path string, body *url.Values) ([]byte, error) {
	var fullPath = endpoint + path

	body.Add("vendor_id", c.vendorID)
	body.Add("vendor_auth_code", c.authToken)

	request, err := http.NewRequest(method, fullPath, strings.NewReader(body.Encode()))
	if err != nil {
		return nil, err
	}
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := c.client.Do(request)
	if err != nil {
		return nil, err
	}

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return result, nil
}

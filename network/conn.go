package network

import (
	"crypto/sha1"
	"encoding/gob"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"github.com/xtaci/kcp-go"
	"golang.org/x/crypto/pbkdf2"
)

// Conn holds information about a UDP connection
type Conn struct {
	secret     string
	session    *kcp.UDPSession
	decoder    *gob.Decoder
	encoder    *gob.Encoder
	remoteAddr string
	counter    uint64
	chanList   sync.Map
}

// NewConnection establishes a secure and authenticated connection to addr
func NewConnection(secret, encPass, encSalt, addr string) (*Conn, error) {
	key := pbkdf2.Key([]byte(encPass), []byte(encSalt), 1024, 32, sha1.New)
	block, err := kcp.NewAESBlockCrypt(key)
	if err != nil {
		return nil, err
	}
	sess, err := kcp.DialWithOptions(addr, block, 10, 3)
	if err != nil {
		return nil, err
	}
	return &Conn{
		secret:     secret,
		session:    sess,
		decoder:    gob.NewDecoder(sess),
		encoder:    gob.NewEncoder(sess),
		remoteAddr: sess.RemoteAddr().(*net.UDPAddr).IP.String(),
	}, nil
}

// NewConnectionFromSession creates a Conn from an already established KCP session
func NewConnectionFromSession(secret string, session *kcp.UDPSession) *Conn {
	return &Conn{
		secret:     secret,
		session:    session,
		decoder:    gob.NewDecoder(session),
		encoder:    gob.NewEncoder(session),
		remoteAddr: session.RemoteAddr().(*net.UDPAddr).IP.String(),
	}
}

func (c *Conn) count() uint64 {
	for {
		val := atomic.LoadUint64(&c.counter)
		if atomic.CompareAndSwapUint64(&c.counter, val, val+1) {
			return val
		}
	}
}

// Send signs, serializes and sends a message. Returns the channel where it will receive the answer
func (c *Conn) Send(msg *Message) (chan Message, error) {
	msg.Counter = c.count()
	signedMsg, err := msg.sign(c.secret)
	if err != nil {
		return nil, err
	}

	err = c.encoder.Encode(signedMsg)
	if err != nil {
		return nil, err
	}

	// The answer for message with ID 'counter' will be received on channel 'ch'
	ch := make(chan Message, 1)
	c.chanList.Store(msg.Counter, ch)

	return ch, nil
}

// Receive unserializes, verifies and returns the received message via its pre-determined channel
func (c *Conn) Receive() error {
	signedMsg := &SignedMessage{}
	if err := c.decoder.Decode(signedMsg); err != nil {
		return err
	}

	msg, err := signedMsg.verify(c.secret)
	if err != nil {
		return err
	}

	ch, ok := c.chanList.Load(msg.Counter)
	if !ok {
		return fmt.Errorf("Can't find channel for message counter %v", msg.Counter)
	}
	c.chanList.Delete(msg.Counter)

	// Channel is buffered so shouldn't block unless an identical ID
	// is used for multiple messages. Just in case we put a timeout
	go func() {
		select {
		case ch.(chan Message) <- *msg:
		case <-time.After(20 * time.Second):
		}
	}()

	return nil
}

// Disconnect closes the listening socket
func (c *Conn) Disconnect() {
	c.chanList.Range(func(key interface{}, value interface{}) bool {
		close(value.(chan Message))
		return true
	})
	c.session.Close()
}

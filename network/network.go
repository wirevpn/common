package network

import "encoding/gob"

func init() {
	gob.Register(&MsgAcquire{})
	gob.Register(&MsgRelease{})
	gob.Register(&MsgHello{})
	gob.Register(&MsgStats{})
	gob.Register(&MsgConfig{})
	gob.Register(&SignedMessage{})
}

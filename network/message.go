package network

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"fmt"
)

// MsgAcquire is an on-demand message sent from server to Janitor that requests a WireGuard
// config data for a peer and also instructs Janitor to make appropriate config modifications
// on the WireGuard side
type MsgAcquire struct {
	JanitorID   string
	PublicKey   string
	AccountPass string
}

// MsgRelease is an on-demand message sent from server to Janitor that requests removal
// of a peer. The request can be made in PublicKey basis or AccountPass basis. All should be true
// when passing AccountPass
type MsgRelease struct {
	All bool
	Key string
}

// MsgHello is sent from Janitor to server once the connection is established
type MsgHello struct {
	JanitorID   string
	Country     string
	WGPublicKey string
	WGPort      string
	WGDNSv4     string
	WGDNSv6     string
}

// MsgStats is a scheduled message sent from Janitor to server that holds information about
// the server and its stats like CPU, Memory usage atc.
type MsgStats struct {
	CPU       float64
	Mem       float64
	Bandwidth float64
	Online    float64
}

// MsgConfig is the response to a MsgAcquire request
type MsgConfig struct {
	JanitorID   string
	Endpoint    string
	LocalIPs    []string
	OnlineCount int
}

// SignedMessage is Message signed with HMAC
type SignedMessage struct {
	Signature string

	// CAUTION: We transport the inner message gobbed as well because different Go versions
	// might have different gob outputs. That would keep us from verifying the signature correctly.
	// This means we have to de-gob twice but it's safer this way.
	// Update: Gobs are backward compatible but that doesn't mean they have to be byte-for-byte
	// identical
	Msg []byte
}

// Message holds information about server <-> Janitor commands
type Message struct {
	Counter uint64
	Data    interface{}
}

// Sign signs a message
func (msg *Message) sign(secret string) (*SignedMessage, error) {
	// Convert message to a gob byte slice
	buf := new(bytes.Buffer)
	if err := gob.NewEncoder(buf).Encode(msg); err != nil {
		return nil, err
	}

	// Calculate message signature
	h := hmac.New(sha256.New, []byte(secret))
	_, err := h.Write(buf.Bytes())
	if err != nil {
		return nil, err
	}

	// Prepare the final signed message as a gob byte slice
	signedMsg := &SignedMessage{
		Signature: hex.EncodeToString(h.Sum(nil)),
		Msg:       buf.Bytes(),
	}

	return signedMsg, nil
}

// Verify verifies the signature of a message
func (s *SignedMessage) verify(secret string) (*Message, error) {
	// Calculate message signature
	h := hmac.New(sha256.New, []byte(secret))
	_, err := h.Write(s.Msg)
	if err != nil {
		return nil, err
	}
	mac2 := h.Sum(nil)

	// Retrieve signature
	mac1, err := hex.DecodeString(s.Signature)
	if err != nil {
		return nil, err
	}

	// Verify
	if !hmac.Equal(mac1, mac2) {
		return nil, fmt.Errorf("Signature verification failed")
	}

	buf := bytes.NewBuffer(s.Msg)
	data := &Message{}
	if err := gob.NewDecoder(buf).Decode(data); err != nil {
		return nil, err
	}
	return data, nil
}

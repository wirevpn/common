# common

Common Go packages shared by various WireVPN projects

* **config** is a parser for WireGuard config files from either a string or filesystem.
* **curve25519** is a small library for generating a base64 encoded private/public key pair to use with WireGuard.
* **randstr** is simple a random string generator with N characters.
* **paddle** is a non-complete paddle.com REST API client.
* **network** is a UDP communication framework specifically tailored for WireVPN.
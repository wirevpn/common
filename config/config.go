package config

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"log"
	"os"
	"strconv"
	"strings"
	"text/template"
)

// Interface holds information about an [Interface] config block
type Interface struct {
	Address    []string
	ListenPort string
	PrivateKey string
	PostUp     string
	PostDown   string
	DNS        []string
	MTU        int
}

// Peer holds information about a [Peer] config block
type Peer struct {
	PublicKey string
	AllowedIP []string
	Endpoint  string
}

// File holds information about a config file
type File struct {
	Interface *Interface
	Peers     []*Peer
}

func parse(scanner *bufio.Scanner) (*File, error) {
	var (
		c          File
		lineNumber int
		lastObject interface{}
		r          = strings.NewReplacer(
			"\t", "",
			"\n", "",
			"\v", "",
			"\f", "",
			"\r", "",
			" ", "",
			string(0x85), "",
			string(0xA0), "",
		)
	)

	for scanner.Scan() {
		var (
			cleanStr = r.Replace(scanner.Text())
			line     = strings.SplitN(cleanStr, "=", 2)
			property = strings.ToLower(line[0])
		)

		lineNumber++

		switch property {
		case "":
			continue

		case "[interface]":
			c.Interface = &Interface{}
			lastObject = c.Interface

		case "[peer]":
			newPeer := &Peer{}
			c.Peers = append(c.Peers, newPeer)
			lastObject = newPeer

		default:
			if len(line) < 2 {
				log.Printf("%d: Property '%s' is empty", lineNumber, property)
				continue
			}
			switch property {
			case "address":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.Address = append(c.Interface.Address, strings.Split(line[1], ",")...)

			case "listenport":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.ListenPort = line[1]

			case "privatekey":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.PrivateKey = line[1]

			case "dns":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.DNS = append(c.Interface.DNS, strings.Split(line[1], ",")...)

			case "mtu":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				imtu, err := strconv.Atoi(line[1])
				if err != nil {
				    log.Printf("%d: Property '%s' can't be parsed", lineNumber, property)
				}
				c.Interface.MTU = imtu

			case "postup":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.PostUp = line[1]

			case "postdown":
				if _, ok := lastObject.(*Interface); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				}
				c.Interface.PostDown = line[1]

			case "allowedips":
				if p, ok := lastObject.(*Peer); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				} else {
					p.AllowedIP = append(p.AllowedIP, strings.Split(line[1], ",")...)
				}

			case "endpoint":
				if p, ok := lastObject.(*Peer); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				} else {
					p.Endpoint = line[1]
				}

			case "publickey":
				if p, ok := lastObject.(*Peer); !ok {
					log.Printf("%d: Property '%s' is under wrong block", lineNumber, property)
					continue
				} else {
					p.PublicKey = line[1]
				}

			default:
				log.Printf("%d: Unsupported property", lineNumber)
				continue
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return &c, nil
}

// Open parses a config file from filesystem
func Open(path string) (*File, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return parse(bufio.NewScanner(file))
}

// OpenString parses a config from a string
func OpenString(cfg string) (*File, error) {
	return parse(bufio.NewScanner(strings.NewReader(cfg)))
}

// Bytes returns a byte slice for the generated config file in wg-quick format
func (f *File) Bytes() ([]byte, error) {
	var tpl bytes.Buffer
	intft := template.Must(template.New("interface").Parse(intfTemplate))
	if err := intft.Execute(&tpl, f.Interface); err != nil {
		return nil, err
	}
	peert := template.Must(template.New("peer").Parse(peerTemplate))
	for _, p := range f.Peers {
		if err := peert.Execute(&tpl, p); err != nil {
			return nil, err
		}
	}
	return tpl.Bytes(), nil
}

// BytesAPI returns a byte slice for the generated config file in key/value format
// It doesn't include the private key for interface since user should provide it
// DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED
// DEPRECATED - Will be removed after mobile app update // DEPRECATED
func (f *File) BytesAPI() []byte {
	var buf bytes.Buffer
	if f.Interface != nil {
		i := f.Interface
		if i.ListenPort != "" {
			buf.WriteString("listen_port=")
			buf.WriteString(i.ListenPort)
			buf.WriteByte('\n')
		}
		for _, a := range i.Address {
			buf.WriteString("address=")
			buf.WriteString(a)
			buf.WriteByte('\n')
		}
		for _, d := range i.DNS {
			buf.WriteString("dns=")
			buf.WriteString(d)
			buf.WriteByte('\n')
		}
	}
	for _, p := range f.Peers {
		if p.PublicKey != "" {
			buf.WriteString("public_key=")
			buf.WriteString(p.PublicKey)
			buf.WriteByte('\n')
		}
		for _, a := range p.AllowedIP {
			buf.WriteString("allowed_ip=")
			buf.WriteString(a)
			buf.WriteByte('\n')
		}
		if p.Endpoint != "" {
			buf.WriteString("endpoint=")
			buf.WriteString(p.Endpoint)
			buf.WriteByte('\n')
		}
	}
	return buf.Bytes()
}

// IPC returns a string of the config in the key/value format understood by wg
func (f *File) IPC() string {
	var buf bytes.Buffer
	if f.Interface != nil {
		i := f.Interface
		if i.PrivateKey != "" {
			buf.WriteString("private_key=")
			buf.WriteString(base64toHex(i.PrivateKey))
			buf.WriteByte('\n')
		}
		if i.ListenPort != "" {
			buf.WriteString("listen_port=")
			buf.WriteString(i.ListenPort)
			buf.WriteByte('\n')
		}
		buf.WriteString("replace_peers=true\n")
	}
	for _, p := range f.Peers {
		if p.PublicKey != "" {
			buf.WriteString("public_key=")
			buf.WriteString(base64toHex(p.PublicKey))
			buf.WriteByte('\n')
		}
		for _, a := range p.AllowedIP {
			buf.WriteString("allowed_ip=")
			buf.WriteString(a)
			buf.WriteByte('\n')
		}
		if p.Endpoint != "" {
			buf.WriteString("endpoint=")
			buf.WriteString(p.Endpoint)
			buf.WriteByte('\n')
		}
	}
	return string(buf.Bytes())
}

func base64toHex(text string) string {
	t, _ := base64.StdEncoding.DecodeString(text)
	return hex.EncodeToString(t)
}

package config

import (
	"fmt"
	"testing"
)

const cfg = `[Interface]
PrivateKey = MFZmnFhhk3TdSAd1f26BHhjuS+NPMVT+7Ou52hRsq3g=
Address = 192.168.0.134/32, fdaa::86/128
DNS = 192.168.0.0, fdaa::

[Peer]
PublicKey = j2zlhzHSTkQwpi52CDdelDUqNXkLPpudxxRkuGXkfC0=
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = test-usa-1.wirevpn.net:51820
`

func TestParse(t *testing.T) {
	p, err := OpenString(cfg)
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	f, err := p.Bytes()
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	fmt.Println(string(f))
	s := p.IPC()
	fmt.Println(s)
}

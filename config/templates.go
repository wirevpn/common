package config

const intfTemplate = "[Interface]" +
	"{{if .PrivateKey}}\nPrivateKey = {{.PrivateKey}}{{end}}" +
	"\nAddress ={{range $index, $element := .Address}}{{if $index}},{{end}} {{$element}}{{end}}" +
	"{{if .ListenPort}}\nListenPort = {{.ListenPort}}{{end}}" +
	"{{if .DNS}}\nDNS ={{range $index, $element := .DNS}}{{if $index}},{{end}} {{$element}}{{end}}{{end}}" +
	"{{if .PostUp}}\nPostUp = {{.PostUp}}{{end}}" +
	"{{if .PostDown}}\nPostDown = {{.PostDown}}{{end}}" +
	"\n"

const peerTemplate = "\n[Peer]" +
	"\nPublicKey = {{.PublicKey}}" +
	"\nAllowedIPs ={{range $index, $element := .AllowedIP}}{{if $index}},{{end}} {{$element}}{{end}}" +
	"{{if .Endpoint}}\nEndpoint = {{.Endpoint}}{{end}}" +
	"\n"
